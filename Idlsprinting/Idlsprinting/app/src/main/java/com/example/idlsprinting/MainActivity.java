package com.example.idlsprinting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static  int SPLASH_TIME=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent= new Intent(MainActivity.this,Main2Activity.class);
                startActivity(intent);


            }
        },SPLASH_TIME);

        TextView textView=(TextView)findViewById(R.id.textview2);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String google="idlabels-001-site1.btempurl.com/index.aspx";
                Uri webadress= Uri.parse(google);
                Intent gotoGoogle=new Intent(Intent.ACTION_VIEW,webadress);

                if(gotoGoogle.resolveActivity(getPackageManager())!=null) {

                    startActivity(gotoGoogle);
                }
            }
        });



    }
}
