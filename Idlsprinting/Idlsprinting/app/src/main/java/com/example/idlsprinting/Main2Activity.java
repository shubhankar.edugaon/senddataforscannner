package com.example.idlsprinting;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Main2Activity extends AppCompatActivity {
    EditText el,ip, port, eventEditText;
    TextView setip,setport, companyName;
    Button saveip,clear;
    Button save, sendData_btn;
    char character = 'a';
    int ascii = (int) character;
    SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        sendData_btn = findViewById(R.id.myButton);
        companyName = findViewById(R.id.company_name);
        eventEditText = findViewById(R.id.editTextPersonName);
        el=(EditText)findViewById(R.id.EditText01);
        ip=(EditText)findViewById(R.id.edtip);
        setip=(TextView)findViewById(R.id.lblip);
        saveip=(Button)findViewById(R.id.save);
        clear=(Button)findViewById(R.id.clearprint);
        setport=(TextView)findViewById(R.id.lblport);
        port = (EditText) findViewById(R.id.editPort);

        sp = getSharedPreferences("myDataPrefs", Context.MODE_PRIVATE);

        String ipAdd = sp.getString("ipAddress","");
        String portNum = sp.getString("portNumber","");
        setip.setText(ipAdd);
        setport.setText(portNum);

       el.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    String message = el.getText().toString().trim();
                    if (message.isEmpty()) {
                        Toast.makeText(Main2Activity.this,"No Data To Send",Toast.LENGTH_SHORT).show();
                    }else {
                        send(v);
                    }
                    return true;
                }
                return false;
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                el.setText("");
            }
        });

        saveip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content=ip.getText().toString();
                String po = port.getText().toString();
                if (content.isEmpty()){
                    Toast.makeText(Main2Activity.this,"ip is not found",Toast.LENGTH_SHORT).show();
                }else  if(po.isEmpty()){
                Toast.makeText(Main2Activity.this,"port is not found",Toast.LENGTH_SHORT).show();
                }else {
                    setport.setText(po);
                    setip.setText(content);
                    ip.setText("");
                    el.requestFocus();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("ipAddress", content);
                    editor.putString("portNumber", po);
                    editor.commit();
                    Toast toast1 = Toast.makeText(getApplicationContext(), "Ip And Port Saved Successfully", Toast.LENGTH_SHORT);
                    toast1.show();
                }
            }
        });

        final Button finish=(Button)findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                moveTaskToBack(true);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if( keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            event.startTracking();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    public void send(View V){
            String output="";

            String prn;
            String str1="";

//            prn="SIZE 98.7 mm, 50 mm\n" +
//                    "GAP 3 mm, 0 mm\n" +
//                    "DIRECTION 0,0\n" +
//                    "REFERENCE 0,0\n" +
//                    "OFFSET 0 mm\n" +
//                    "SET PEEL OFF\n" +
//                    "SET CUTTER OFF\n" +
//                    "SET PARTIAL_CUTTER OFF\n" +
//                    "SET TEAR ON\n" +
//                    "CLS\n" +
//                    "BARCODE 596,255,\"128M\",80,1,180,3,6,\"<FE/>Barcode</FE>\"\n" +
//                    "PRINT 1,1\n";

            MessageSender messageSender= new   MessageSender();
            // String str= el.getText().toString();
//            str1=prn;
          //  prn=prn.replace("<FE/>Barcode</FE>",el.getText().toString());\
        String message = el.getText().toString().trim();
        if (message.isEmpty()){
            Toast.makeText(Main2Activity.this,"No Data To Send",Toast.LENGTH_SHORT).show();
        }else {
            output = el.getText().toString() + "\n";
            messageSender.execute(output);
            Toast toast = Toast.makeText(getApplicationContext(), "Data Sent...", Toast.LENGTH_SHORT);
            toast.show();
            el.getText().clear();
        }
    }
    public class MessageSender extends AsyncTask<String,Void,Void> {

        Socket s;
        PrintWriter pw;
        DataOutputStream dos;
        @Override
        protected Void doInBackground(String... voids) {
            String message=voids[0];

            String ipvalue= setip.getText().toString();
            String portvalue=setport.getText().toString();
            try {
                s= new Socket(ipvalue, Integer.parseInt(portvalue));
                pw= new PrintWriter(s.getOutputStream());
                pw.write(message);
                pw.flush();
                pw.close();
                s.close();
                el.clearFocus();

            }catch (IOException exception){
                exception.printStackTrace();
            }

            return null;
        }
    }

    public void showSoftKeyboard(View view) {
        if (view.requestFocus()) {InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    }

